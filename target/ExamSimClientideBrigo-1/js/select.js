function queryDB(category){
//	console.log(category);
	var request = new XMLHttpRequest();
	var resultsDiv = document.getElementById("results");
	resultsDiv.innerHTML="";
	request.onload = function(){
		var list = JSON.parse(request.responseText);
		if(list["message"]!=undefined){
			console.log(list);
			var alert = document.createElement('div')
			alert.setAttribute('class','alert alert-warning');
			var ul = document.createElement('ul');
			var li1 = document.createElement('li');
			li1.innerHTML="<b>Message: </b>"+list["message"].message;
			var li2 = document.createElement('li');
			li2.innerHTML="<b>Error code: </b>"+list["message"]["error-code"];
			var li3 = document.createElement('li');
			li3.innerHTML="<b>Error details: </b>"+list["message"]["error-details"];			
			ul.appendChild(li1);
			ul.appendChild(li2);
			ul.appendChild(li3);
			alert.appendChild(ul);
			resultsDiv.appendChild(alert);
			
		}
		else{
		if(list["resource-list"].length==0)
			resultsDiv.innerHTML="NO RESULTS";
			//console.log("No results");
		else{
		//TABLE INITIALIZATION AND TABLE HEAD CREATION
		var tbl=document.createElement('table');
		tbl.setAttribute('class', 'table table-sm table-hover');
		tbl.setAttribute('id', 'active_table');
		var thead = document.createElement('thead');  
		var tr= document.createElement('tr');
		var th1 = document.createElement('th');
		th1.appendChild(document.createTextNode("ID"));
		tr.appendChild(th1);
		var th2 = document.createElement('th');
		th2.appendChild(document.createTextNode("Category"));
		tr.appendChild(th2);
		var th3 = document.createElement('th');
		th3.appendChild(document.createTextNode("Name"));
		tr.appendChild(th3);
		var th4 = document.createElement('th');
		th4.appendChild(document.createTextNode("Age Range"));
		tr.appendChild(th4);
		var th5 = document.createElement('th');
		th5.appendChild(document.createTextNode("Price"));
		tr.appendChild(th5);
		thead.appendChild(tr);
		tbl.appendChild(thead);
		var tbdy = document.createElement('tbody');
		for(i=0;i<list["resource-list"].length;i++){
					var tr= document.createElement('tr');
					var td1 = document.createElement('td');
					td1.appendChild(document.createTextNode(list["resource-list"][i].product.id));
					tr.appendChild(td1);
					var td2 = document.createElement('td');
					td2.appendChild(document.createTextNode(list["resource-list"][i].product.category));
					tr.appendChild(td2);
					var td3 = document.createElement('td');
					td3.appendChild(document.createTextNode(list["resource-list"][i].product.name));
					tr.appendChild(td3);
					var td4 = document.createElement('td');
					td4.appendChild(document.createTextNode(list["resource-list"][i].product.ageRange));
					tr.appendChild(td4);
					var td5 = document.createElement('td');
					td5.appendChild(document.createTextNode(list["resource-list"][i].product.price));
					tr.appendChild(td5);
					tbdy.appendChild(tr);

				console.log(list["resource-list"][i].product.id+list["resource-list"][i].product.category+list["resource-list"][i].product.name+list["resource-list"][i].product.ageRange+list["resource-list"][i].product.price);
		}
		tbl.appendChild(tbdy);
		resultsDiv.appendChild(tbl);
		}
	}
	}
	request.open('GET', '../ExamSimClientideBrigo-1/search-product-by-category?category='+category);
	request.send();
	
}
