/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.resource;

/**
 * Represents a Toy.
 *
 * @author Francesco Brigo
 * @version 1.00
 * @since 1.00
 */
public class Toy {

    /**
     * The id of the toy.
     */
    private int id;

    /**
     * The name of the toy.
     */
    private String name;
    /**
     * The category of the toy.
     */
    private String category;
    /**
     * The price of the toy.
     */
    private double price;
    /**
     * The age range of the toy.
     */
    private String ageRange;


    /**
     * Creates a toy.
     * @param id id of the toy
     * @param name name of the toy
     * @param category category of the toy
     * @param price price of the toy
     * @param ageRange ageRange of the toy
     */
    public Toy(int id, String name, String category, double price, String ageRange){
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.ageRange = ageRange;
    }

    /**
     * Returns the id of the toy.
     * @return the id of the toy.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the name of the toy.
     * @return the name of the toy.
     */
    public String getName() {
        return name;
    }
    /**
     * Returns the price of the toy.
     * @return the price of the toy.
     */
    public double getPrice() {
        return price;
    }
    /**
     * Returns the category of the toy.
     * @return the category of the toy.
     */
    public String getCategory() {
        return category;
    }
    /**
     * Returns the category of the toy.
     * @return the category of the toy.
     */
    public String getAgeRange() {
        return ageRange;
    }
}
