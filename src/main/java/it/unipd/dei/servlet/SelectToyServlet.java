package it.unipd.dei.servlet;

import it.unipd.dei.bcs.webapp.AbstractDatabaseServlet;
import it.unipd.dei.database.pullToyDatabase;
import it.unipd.dei.resource.Toy;
import it.unipd.dei.resource.Message;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Retrieves the logs associated to a specific user from the database.
 *
 * @author BCS
 * @version 1.00
 * @since 1.00
 */
public class SelectToyServlet extends AbstractDatabaseServlet {

    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        doPost(req,res);
    }

    /**
     * Retrieves logs related to a user from the database.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        // request parameters
        String category = null;

        // model
        Message m = null;
        ArrayList<Toy> toys = null;

        try{
            // retrieves the request parameters
            category = req.getParameter("category");

            // creates a new object for accessing the database and stores the User
            toys = (new pullToyDatabase(getDataSource().getConnection(),category)).queryToySelect();

            m = new Message(String.format("Successfully retrieved info of the selected category"));

        } catch (NumberFormatException ex) {
            m = new Message("Cannot search for products: unexpected error while accessing the database.",
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            m = new Message("Cannot search for products: unexpected error while accessing the database.",
                    "E100", ex.getMessage());
        }

        // set the MIME media type of the response, in our case is JSON because of frontend implementation
        res.setContentType("application/json; charset=utf-8");
        // get a stream to write the response
        PrintWriter out = res.getWriter();
        // Decision about how to implement the error message

        if(m.isError()) {
            JSONObject errorMessage = new JSONObject();
            JSONObject msg = new JSONObject();
            msg.put("message",m.getMessage());
            msg.put("error-code",m.getErrorCode());
            msg.put("error-details",m.getErrorDetails());
            errorMessage.put("message",msg);
            out.printf(errorMessage.toJSONString());
        } else {
            JSONObject toysJSON = new JSONObject();
            JSONArray toysList = new JSONArray();
            if(toys!=null) {
                for (Toy t : toys) {
                    JSONObject product = new JSONObject();
                    JSONObject toy = new JSONObject();
                    toy.put("id", t.getId());
                    toy.put("category", t.getCategory());
                    toy.put("name",t.getName());
                    toy.put("ageRange",t.getAgeRange());
                    toy.put("price",t.getPrice());
                    product.put("product",toy);
                    toysList.add(product);
                }
            }
            toysJSON.put("resource-list",toysList);
            out.printf(toysJSON.toJSONString());
        }
        // flush the output stream buffer
        out.flush();

        // close the output stream
        out.close();
    }
}
