package it.unipd.dei.database;

import it.unipd.dei.resource.Toy;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Retrieves informations from the toy table of the database.
 *
 * @author Brigo Francesco
 * @version 1.00
 * @since 1.00
 */
public class pullToyDatabase {

    /**
     * The SQL statement to retrieve logs associated to a user
     */
    private static final String SELECT_STATEMENT = "SELECT * FROM mytoys.product WHERE category=?";

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * The user who does the actions to be retrieved from the database
     */
    private final String category;

    /**
     * Searches for logs associated to the username into the database.
     *
     * @param con the connection to the database.
     * @param category the username to be searched into the database.
     */
    public pullToyDatabase(final Connection con, final String category) {
        this.con = con;
        this.category = category;
    }

    /**
     * Retrieves info from the log table
     *
     * @throws SQLException if any error occurs while retrieving the user logs.
     * @return an array of logs.
     */
    public ArrayList<Toy> queryToySelect() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Toy> toys = new ArrayList<>();

        try {
            pstmt = con.prepareStatement(SELECT_STATEMENT);
            pstmt.setString(1, category);
            rs = pstmt.executeQuery();
            while(rs.next()){
        		toys.add(new Toy(rs.getInt("id"),rs.getString("name"),rs.getString("category"),rs.getDouble("price"),rs.getString("ageRange")));
            }
        } finally {
            if(rs != null){
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            con.close();
        }
        return toys;
    }
}
